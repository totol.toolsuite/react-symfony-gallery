import { NavLink } from "react-router-dom"

function GalleryLinkItem({name, id}) {
  return (
    <>
      <div className="w-full flex gap-4 items-center">
        <span>{name}</span>
        <NavLink to={`/galleries/${id}`}>Lien vers la gallerie</NavLink>
        <NavLink to={`/admin/edit/${id}`}>Admin</NavLink>
      </div>
    </>
  )
}

export default GalleryLinkItem
