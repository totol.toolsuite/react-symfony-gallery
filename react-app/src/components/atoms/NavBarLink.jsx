import { NavLink } from "react-router-dom"

function NavBarLink({name, link}) {
  return (
    <>
      <NavLink to={link}>{name}</NavLink>
    </>
  )
}

export default NavBarLink
