import { NavLink } from "react-router-dom"
import { getGalleries } from "../../services/galleries"
import { useEffect, useState } from "react"
import GalleryLinkItem from "../atoms/GalleryLinkItem"

function GalleriesList() {
  const [galleries, setGalleries] = useState([])

  const fetchGalleries = async () => {
    const fetched = await getGalleries()
    setGalleries(fetched)
  }

  useEffect(() => {
    fetchGalleries()
  }, [])


  return (
    <>
      <div >
        <h1>Liste des galleries</h1>
        <div className="flex flex-col">
          {galleries.map(x => <GalleryLinkItem key={x.id} name={x.title} id={x.id}></GalleryLinkItem>)}
        </div>
      </div>
    </>
  )
}

export default GalleriesList
