import { NavLink } from "react-router-dom"
import NavBarLink from "../atoms/NavBarLink"

function NavBar() {
  return (
    <>
      <div className="w-full h-8 bg-green-200 flex justify-center items-center gap-4">
        <NavBarLink name="Home" link={'/'}></NavBarLink>
        <NavBarLink name="Admin" link={'/admin'}></NavBarLink>
      </div>
    </>
  )
}

export default NavBar
