import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import {
  RouterProvider,
} from "react-router-dom";
import { router } from './utils/router';
import { ErrorBoundary } from 'react-error-boundary';


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <ErrorBoundary 
        fallback={<div>Une erreur est survenue</div>}>
      <RouterProvider router={router}></RouterProvider>
    </ErrorBoundary>    
  </React.StrictMode>,
)
