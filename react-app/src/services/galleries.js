import apiClient from '@/utils/apiClient'

const entityPath = 'galleries'

export async function getGalleries() {
  try {
    const galleries = await apiClient.get(entityPath)
    return galleries.data['hydra:member']
  } catch {
    return []
  }
}

export async function showGallery(id) {
  try {
    const gallery = await apiClient.get(`${entityPath}/${id}`)
    return gallery.data
  } catch {
    return null
  }
}