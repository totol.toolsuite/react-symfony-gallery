import { createBrowserRouter } from "react-router-dom";
import Admin from "../views/Admin";
import Home from "../views/Home";
import EditGallery from "../views/EditGallery";
import Gallery from "../views/Gallery";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <Home></Home>,
  },
  {
    path: "/admin",
    element: <Admin></Admin>,
  },
  {
    path: "/admin/edit/:id",
    element: <EditGallery></EditGallery>,
  },
  {
    path: "/galleries/:id",
    element: <Gallery></Gallery>,
  },
]);