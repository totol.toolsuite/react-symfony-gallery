import GalleriesList from "../components/molecules/GalleriesList"
import NavBar from "../components/molecules/NavBar"

function Admin() {
  return (
    <>
      <NavBar></NavBar>
      <h1 className="text-blue-500 underline">ADMIN</h1>
      <GalleriesList></GalleriesList>
    </>
  )
}

export default Admin
