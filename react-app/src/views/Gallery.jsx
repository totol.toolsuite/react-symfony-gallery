import { useParams, useRoutes } from "react-router"
import NavBar from "../components/molecules/NavBar"
import { showGallery } from "../services/galleries"
import { useEffect, useState } from "react"

function Gallery() {
  const params = useParams()
  

  const [gallery, setGallery] = useState([])

  const fetchGallery = async () => {
    const fetched = await showGallery(params.id)
    setGallery(fetched)
  }

  useEffect(() => {
    fetchGallery()
  }, [])


  return (
    <>
      <NavBar></NavBar>
      <h1>Gallery</h1>
      {JSON.stringify(gallery)}
    </>
  )
}

export default Gallery
