import NavBar from "../components/molecules/NavBar"

function Home() {
  return (
    <>
      <NavBar></NavBar>
      <h1 className="text-blue-500 underline">Home</h1>
    </>
  )
}

export default Home
